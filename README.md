# This repo was originally hosted on GitHub which removed it. I rescued it from an archive and present it here for your enjoyment. *-[Ben Piper](https://benpiper.com)*

# Skiptracer - OSINT scraping framework
![python](https://img.shields.io/badge/python-2.7-green.svg) ![version](https://img.shields.io/badge/version-0.2.0-brightgreen.svg) ![licence](https://img.shields.io/badge/license-GPLv3-lightgrey.svg) 

![screen](https://i.imgur.com/gG0KZ0F.png)

Initial attack vectors for recon usually involve utilizing pay-for-data/API (Recon-NG), or paying to utilize transforms (Maltego) to get data mining results. Skiptracer utilizes some basic python webscraping (BeautifulSoup) of PII paywall sites to compile passive information on a target on a ramen noodle budget.

Example:
----

[![asciicast](https://asciinema.org/a/RosGkr3mie2s6hjwUJC1TT2lJ.png)](https://asciinema.org/a/RosGkr3mie2s6hjwUJC1TT2lJ)


## Installation and Usage

```
$ git clone https://bitbucket.org/benpiper/xillwillx-skiptracer.git
$ cd xillwillx-skiptracer
```
__Install requirements__
```
$ pip install -r requirements.txt
```
__Run__
```
$ python skiptracer.py
```

## Instructions for OSX

This has been tested on OSX High Sierra 10.13.4. This installation requires brew and pipenv. If you've already got brew installed, skip those two steps.

Install Brew(Skip if you have it installed already) and pipenv:

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install pipenv
Clone Skiptracer:

git clone https://github.com/xillwillx/skiptracer.git skiptracer

Install pipenv and spawn a pipenv virtual environment to run Skiptracer:

cd skiptracer
pipenv install -r requirements.txt
pipenv --python 2.7
pipenv shell
From this new pipenv virtual environment, you can run Skiptracer with all necessary dependencies.

### Dockerized Environment

You may run this application in a dockerized environment using the command:

docker-compose run --rm skiptracer
Note: the --rm flag will remove the container after execution.